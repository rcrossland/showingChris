"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tileEnum_1 = require("./tileEnum");
var _ = require("lodash");
var Board = /** @class */ (function () {
    function Board(width, height, numBombs) {
        this._tiles = [];
        this._width = width && width > 0 ? width : 1;
        this._height = height > 0 ? height : 1;
        this._numBombs =
            numBombs > 0 && numBombs < (this._width * this._height) ? numBombs : 1;
    }
    Board.prototype.populate = function () {
        this._layBombs();
        this._laySpaces();
        this._shuffle();
        this._calculateTiles();
        this._show();
    };
    Board.prototype._layBombs = function () {
        for (var i = 0; i < this._numBombs; i++) {
            this._tiles.push(tileEnum_1.TileEnum.Bomb);
        }
    };
    Board.prototype._laySpaces = function () {
        var spacesWithoutBombs = (this._width * this._height) - this._numBombs;
        for (var i = 0; i < spacesWithoutBombs; i++) {
            this._tiles.push(0);
        }
    };
    Board.prototype._shuffle = function () {
        this._tiles = _.shuffle(this._tiles);
    };
    Board.prototype._calculateTiles = function () {
        for (var i = 0; i < (this._width * this._height); i++) {
            if (this._indexIsBomb(i)) {
                continue;
            }
            else {
                this._setNumberOfSurroundingBombs(i);
            }
        }
    };
    Board.prototype._setNumberOfSurroundingBombs = function (currentIndex) {
        this._tiles[currentIndex] =
            this._indexIsBomb(currentIndex + this._width - 1) +
                this._indexIsBomb(currentIndex + this._width) +
                this._indexIsBomb(currentIndex + this._width + 1) +
                this._indexIsBomb(currentIndex - 1) +
                this._indexIsBomb(currentIndex + 1) +
                this._indexIsBomb(currentIndex - this._width - 1) +
                this._indexIsBomb(currentIndex - this._width) +
                this._indexIsBomb(currentIndex - this._width + 1);
    };
    Board.prototype._indexIsBomb = function (index) {
        return index > 0
            && index < (this._width * this._height)
            && this._tiles[index] === tileEnum_1.TileEnum.Bomb
            ? 1 : 0;
    };
    Board.prototype._show = function () {
        console.dir(this._tiles);
    };
    return Board;
}());
exports.Board = Board;
//# sourceMappingURL=board.js.map