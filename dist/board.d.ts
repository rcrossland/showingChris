export declare class Board {
    private _tiles;
    private _width;
    private _height;
    private _numBombs;
    constructor(width: number, height: number, numBombs: number);
    populate(): void;
    private _layBombs;
    private _laySpaces;
    private _shuffle;
    private _calculateTiles;
    private _setNumberOfSurroundingBombs;
    private _indexIsBomb;
    private _show;
}
