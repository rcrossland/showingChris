"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var board_1 = require("./board");
var Minesweeper = /** @class */ (function () {
    function Minesweeper(width, height, numBombs) {
        this._board = new board_1.Board(width, height, numBombs);
        this._board.populate();
    }
    return Minesweeper;
}());
exports.Minesweeper = Minesweeper;
var mineSweeper = new Minesweeper(30, 16, 99);
//# sourceMappingURL=minesweeper.js.map