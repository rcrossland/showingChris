import { TileEnum } from './tileEnum';

export interface Tile {
    visible: boolean;
    value: TileEnum | number;
}