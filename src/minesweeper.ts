import { Board } from './board';

export class Minesweeper {
    private _board: Board;
    
    constructor(width: number, height: number, numBombs: number) {
        this._board = new Board(width, height, numBombs);
        this._board.populate();
        this._board.render();   
    }
}

const mineSweeper = new Minesweeper(30, 16, 99);