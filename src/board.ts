import { Tile } from './tile';
import { TileEnum } from './tileEnum';
import * as _ from 'lodash';

export class Board {
    private _tiles: Array<Tile> = [];
    private _width: number;
    private _height: number;
    private _numBombs: number;
    
    constructor(width: number, height: number, numBombs: number) {
        this._width = width && width > 0 ? width : 1;
        this._height = height > 0 ? height: 1;
        this._numBombs = 
            numBombs > 0 && numBombs < (this._width * this._height) ? numBombs : 1;
    }

    get width(): number {
        return this._width;
    }

    get height() : number {
        return this._height;
    }

    get tiles() : Array<Tile> {
        return this._tiles;
    }

    populate() {
        this._layBombs();
        this._laySpaces();
        this._shuffle();
        this._calculateTiles();
    }

    private _layBombs(): void {
        for(let i = 0; i < this._numBombs; i++) {
            this._tiles.push({
                visible: false,
                value: TileEnum.Bomb
            });
        }
    }

    private _laySpaces(): void {
        const spacesWithoutBombs = (this._width * this._height) - this._numBombs;
        
        for(let i = 0; i < spacesWithoutBombs; i++) {
            this._tiles.push({
                visible: false,
                value: 0
            });
        }
    }

    private _shuffle(): void {
        this._tiles = _.shuffle(this._tiles);
    }

    private _calculateTiles(): void {
        for(let i = 0; i < (this._width * this._height); i++) {
            if(this._indexIsBomb(i, false)) {
                continue;
            } else {
                this._setNumberOfSurroundingBombs(i);
            }
        }
    }

    private _setNumberOfSurroundingBombs(currentIndex: number) {
        this._tiles[currentIndex].value = 
            this._indexIsBomb(currentIndex + this._width - 1, this._isLeftEdge(currentIndex)) +
            this._indexIsBomb(currentIndex + this._width, false) +
            this._indexIsBomb(currentIndex + this._width + 1, this._isRightEdge(currentIndex)) + 
            this._indexIsBomb(currentIndex - 1, this._isLeftEdge(currentIndex)) +
            this._indexIsBomb(currentIndex + 1, this._isRightEdge(currentIndex)) +
            this._indexIsBomb(currentIndex - this._width - 1, this._isLeftEdge(currentIndex)) +
            this._indexIsBomb(currentIndex - this._width, false) +
            this._indexIsBomb(currentIndex - this._width + 1, this._isRightEdge(currentIndex));
    }

    private _isLeftEdge(index: number) {
        return index % this._width === 0;
    }

    private _isRightEdge(index: number) {
        return index % this._width === this._width - 1;
    }

    private _indexIsBomb(index: number, edgeFlag: boolean) {
        return !edgeFlag 
            && index > 0 
            && index < (this._width * this._height)
            && this._tiles[index].value === TileEnum.Bomb
            ? 1 : 0;
    }

    render() {
        const divElement = document.getElementById('minesweeperDiv');
        const table = document.createElement('table');

        for(let row = 0; row < this._height; row++) {
            const tr = table.insertRow();

            for(let col = 0; col < this.width; col++) {
                const td = tr.insertCell();
                this._configureCell(row, col, td);
                tr.appendChild(td);
            }
        }
        
        if(divElement) {
            divElement.innerHTML = '';
            divElement.appendChild(table);
        }
    }

    _configureCell(row: number, col: number, td: HTMLTableCellElement) {
        const cell = this._tiles[row * this._width + col];

        if(cell.visible) {
            if(this._tiles[row * this._width + col].value > 0 && this._tiles[row * this._width + col].value < 9) {
                const textNode = document.createTextNode(this._tiles[row * this._width + col].value.toString());
                td.appendChild(textNode);
            }

            switch(cell.value) {
                case 1:
                    td.className = 'one';
                    break;
                case 2:
                    td.className = 'two';
                    break;
                case 3:
                    td.className = 'three';
                    break;
                case 4:
                    td.className = 'four';
                    break;
                case 5:
                    td.className = 'five';
                    break;
                case 6:
                    td.className = 'six';
                    break;
                case 7:
                    td.className = 'seven';
                    break;
                case 8:
                    td.className = 'eight';
                    break;
                case TileEnum.Bomb:
                    td.className = 'bomb';
                    break;
            }
        } else {
            td.className = 'invisible';

            td.onclick = (e: any) => {
                this._tiles[e.srcElement.parentNode.rowIndex * this._width + e.srcElement.cellIndex].visible = true;
                this.render();
            };
        }
    }
}